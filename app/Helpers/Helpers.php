<?php
/**
 * Create API response
 **/
function createResponseData($code, $message = '', $data = [], $pagination = false, Illuminate\Http\Request $request)
{
    $response = [];
    $response['status_code'] = $code;
    $response['message'] = $message;
    $response['data'] = $data;

    if($pagination){
        $response['pages'] = $pagination;
    }

    return response()->json($response, $code);
}
