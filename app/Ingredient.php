<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ingredient extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'name', 'measure', 'supplier_id' ];

    /**
     * Get ingredient supplier
     */
    public function supplier()
    {
        return $this->belongsTo(Supplier::class);
    }
}
