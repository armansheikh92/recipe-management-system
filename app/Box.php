<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Box extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'delivery_date', 'customer_id' ];

    /**
     * Get box recipes
     */
    public function boxRecipes()
    {
        return $this->hasMany(BoxRecipe::class);
    }

    /**
     * Get box customer
     */
    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }
}
