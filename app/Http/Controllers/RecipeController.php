<?php

namespace App\Http\Controllers;

use App\Recipe;
use App\RecipeIngredient;
use Illuminate\Http\Request;
use Validator;

class RecipeController extends Controller
{
    /**
     * Get ingredients
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $statusCode = 200;
        $message = '';

        //pagination params
        $page  = $request->get('page', 1);
        $size = $request->get('size', 12);

        $pagination = [
            'page'=> $page,
            'size'=> $size
        ];

        //get records with pagination
        $results = Recipe::with('recipeIngredients', 'recipeIngredients.ingredient')->paginate($size);
        $data = $results->items();
        $pagination['total_records'] = $results->total();
        $pagination['total_pages'] = ceil($results->total()/$page);

        return createResponseData($statusCode, $message, $data, $pagination, $request);
    }

    /**
     * Create recipes
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request)
    {
        $data = [];
        $statusCode = 201;
        $message = 'Recipe has been created successfully.';

        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'description' => 'required',
            'ingredients' => 'array',
            'ingredients.*.ingredient_id' => 'required|numeric|exists:ingredients,id',
            'ingredients.*.quantity'      => 'required|numeric'
        ]);

        if ($validator->fails()) {
            $statusCode = 422;
            $message = $validator->errors();
        } else {
            $recipeItem = Recipe::create([
                'name' => $request->get('name'),
                'description' => $request->get('description')
            ]);

            $recipeIngredients = Collect($request->get('ingredients'))->map(function($item){
                return new RecipeIngredient($item);
            });

            $recipeItem->recipeIngredients = $recipeItem->recipeIngredients()->saveMany($recipeIngredients);
            $data = $recipeItem;
        }

        return createResponseData($statusCode, $message, $data, false, $request);
    }
}
