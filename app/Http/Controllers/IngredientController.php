<?php

namespace App\Http\Controllers;

use App\Recipe;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\PaginatedResourceResponse;
use Validator;
use App\Ingredient;
use App\Box;
use Carbon\Carbon;

class IngredientController extends Controller
{
    /**
     * Get ingredients
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $statusCode = 200;
        $message = '';

        //pagination params
        $page  = $request->get('page', 1);
        $size = $request->get('size', 12);

        $pagination = [
            'page'=> $page,
            'size'=> $size
        ];

        //get records with pagination
        $results = Ingredient::with('recipeIngredients')->paginate($size);
        $data = $results->items();
        $pagination['total_records'] = $results->total();
        $pagination['total_pages'] = ceil($results->total()/$page);

        return createResponseData($statusCode, $message, $data, $pagination, $request);
    }

    /**
     * Creates ingredient
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request)
    {
        $data = [];
        $statusCode = 201;
        $message = 'Ingredient has been created successfully.';

        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'measure' => 'required|alpha_num|max:255',
            'supplier_id' => 'required|numeric|exists:suppliers,id'
        ]);

        if ($validator->fails()) {
            $statusCode = 422;
            $message = $validator->errors();
        } else {
            $data = Ingredient::create([
                'name' => $request->get('name'),
                'measure' => $request->get('measure'),
                'supplier_id' => $request->get('supplier_id')
            ]);
        }

        return createResponseData($statusCode, $message, $data, false, $request);
    }

    /**
     * Generate Purchase order for next 7 days
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function generatePurchaseOrder(Request $request)
    {
        $data = [];
        $statusCode = 200;
        $message = 'Purchase order generated.';

        $validator = Validator::make($request->all(), [
            'order_date' => 'required|date|after_or_equal:today'
        ]);

        if ($validator->fails()) {
            $statusCode = 422;
            $message = $validator->errors();
        } else {
            //format and get +7 days date
            $dateTo = Carbon::parse($request->get('order_date'))->add(7, 'day')->format('Y-m-d');
            $dateFrom = Carbon::parse( $request->get('order_date'))->format('Y-m-d');

            //get box recipes count within next 7 days of order date
            $boxRecipes = Box::join('box_recipes', 'box_recipes.box_id', '=', 'boxes.id')->whereBetween('boxes.delivery_date',array($dateFrom,$dateTo))
                ->groupBy('box_recipes.recipe_id')
                ->selectRaw('box_recipes.recipe_id, COUNT(box_recipes.recipe_id) as recipe_counts')
                ->get();

            $ingredients = [];
            $boxRecipes->map(function($item) USE(&$ingredients) {
                $recipes = Recipe::with('recipeIngredients', 'recipeIngredients.ingredient')->where('id', $item->recipe_id)->first();
                $recipeIngredients = $recipes->recipeIngredients;
                $recipeIngredients->map(function($recipeIngredient) USE(&$ingredients, $item) {
                    $ingredient = $recipeIngredient->ingredient;
                    $requiredQuantity = ($item->recipe_counts * $recipeIngredient->quantity);

                    if (!isset($ingredients[$ingredient->id]['required_quantity'])) {
                        $ingredients[$ingredient->id]['required_quantity'] = 0;
                    }

                    $ingredients[$ingredient->id]['name'] = $ingredient->name;
                    $ingredients[$ingredient->id]['measure'] = $ingredient->measure;
                    $ingredients[$ingredient->id]['required_quantity'] = $requiredQuantity;
                });

            });


            $data = $ingredients;
        }

        return createResponseData($statusCode, $message, $data, false, $request);
    }
}
