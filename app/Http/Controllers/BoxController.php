<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Box;
use App\Customer;
use App\BoxRecipe;

class BoxController extends Controller
{
    public function create(Request $request)
    {
        $data = [];
        $statusCode = 201;
        $message = 'Box has been created successfully.';

        $validator = Validator::make($request->all(), [
            'delivery_date' => 'required|date|after:tomorrow',
            'recipes' => 'required|array|max:4',
            'recipes.*' => 'required|numeric|exists:recipes,id',
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email',
            'contact' => 'required|min:9,max:11'
        ]);

        if ($validator->fails()) {
            $statusCode = 422;
            $message = $validator->errors();
        } else {

            $itemCustomer = Customer::create( $request->only('first_name', 'last_name', 'email', 'contact' ) );

            $boxItem = Box::create([
                'delivery_date' => $request->get('delivery_date'),
                'customer_id' => $itemCustomer->id
            ]);

            $boxRecipes = Collect($request->get('recipes'))->map(function($item){
                return new BoxRecipe([ 'recipe_id' => $item ]);
            });

            $boxItem->boxRecipes = $boxItem->boxRecipes()->saveMany($boxRecipes);
            $data = $boxItem;
        }

        return createResponseData($statusCode, $message, $data, false, $request);
    }
}
