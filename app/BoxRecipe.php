<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BoxRecipe extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'recipe_id', 'box_id' ];

    /**
     * Get Recipes
     */
    public function recipes()
    {
        return $this->belongsTo(Recipe::class);
    }
}
