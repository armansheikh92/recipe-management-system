<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->group(['middleware' => 'auth'], function () use ($router) {

    $router->group(['prefix' => 'ingredients'], function () use ($router) {
        $router->post('create', 'IngredientController@create');
        $router->get('/', 'IngredientController@index');
        $router->get('/pruchase-order/generate', 'IngredientController@generatePurchaseOrder');
    });

    $router->group(['prefix' => 'recipes'], function () use ($router) {
        $router->post('create', 'RecipeController@create');
        $router->get('/', 'RecipeController@index');
    });

    $router->group(['prefix' => 'boxes'], function () use ($router) {
        $router->post('create', 'BoxController@create');
    });

});
